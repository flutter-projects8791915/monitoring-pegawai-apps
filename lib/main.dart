import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:monitoring_pegawai/model/user.dart';
import 'package:monitoring_pegawai/page/admin/dashboard_admin.dart';
import 'package:monitoring_pegawai/page/login.dart';
import 'package:monitoring_pegawai/page/operator/dashboard_operator.dart';
import 'package:monitoring_pegawai/page/pegawai/dashboard_pegawai.dart';
import 'event/event_pref.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: FutureBuilder(
        future: EventPref.getUser(),
        builder: (context, AsyncSnapshot<User?> snapshot) {
          return snapshot.data == null
              ? Login()
              : snapshot.data!.role == 'Pegawai'
                  ? DashboardPegawai()
                  : snapshot.data!.role == 'Operator'
                      ? DashboardOperator()
                      : DashboardAdmin();
        },
      ),
    );
  }
}
