import 'package:flutter/material.dart';

class Asset {
  static Color colorPrimaryDark = const Color(0xffdf721c);
  static Color colorPrimary = const Color(0xffffb741);
  static Color colorSecondary = const Color(0xfffde49c);
  static Color colorAccent = const Color(0xff65c9cf);
}
