class User {
  String? idUser;
  String? name;
  String? nohp;
  String? pass;
  String? role;
  String? address;
  String? idJob;
  String? jobName;
  String? salary;

  User({
    this.idUser,
    this.name,
    this.nohp,
    this.address,
    this.idJob,
    this.jobName,
    this.pass,
    this.role,
    this.salary,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        address: json['address'],
        idJob: json['id_Job'],
        idUser: json['id_user'],
        jobName: json['job_name'],
        name: json['name'],
        nohp: json['nohp'],
        pass: json['pass'],
        role: json['role'],
        salary: json['salary'],
      );

  Map<String, dynamic> toJson() => {
        'address': address,
        'id_job': idJob,
        'id_user': idUser,
        'job_name': jobName,
        'name': name,
        'nohp': nohp,
        'pass': pass,
        'role': role,
        'salary': salary
      };
}
