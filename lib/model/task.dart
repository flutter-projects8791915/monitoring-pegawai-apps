class Task {
  String? idTask;
  String? taskName;
  String? description;
  String? progress;
  String? idUser;

  Task({
    this.idTask,
    this.taskName,
    this.description,
    this.progress,
    this.idUser,
  });

  factory Task.fromJson(Map<String, dynamic> json) => Task(
        idTask: json['id_task'],
        taskName: json['task_name'],
        description: json['description'],
        progress: json['progress'],
        idUser: json['id_user'],
      );

  Map<String, dynamic> toJson() => {
        'id_task': idTask,
        'task_name': taskName,
        'description': description,
        'progress': progress,
        'id_user': idUser,
      };
}
