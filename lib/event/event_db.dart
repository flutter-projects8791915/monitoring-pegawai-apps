import 'dart:convert';

import 'package:get/get.dart';
import 'package:monitoring_pegawai/event/event_pref.dart';

import '../config/api.dart';
import '../model/user.dart';
import 'package:http/http.dart' as http;

import '../page/admin/dashboard_admin.dart';
import '../page/operator/dashboard_operator.dart';
import '../page/pegawai/dashboard_pegawai.dart';
import '../widget/info.dart';

class EventDB {
  static Future<User?> login(String nohp, String pass) async {
    User? user;
    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'nohp': nohp,
        'pass': pass,
      });
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        if (responseBody['success']) {
          user = User.fromJson(responseBody['user']);
          EventPref.saveUser(user);
          Info.snackBar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 1700), () {
            Get.off(
              user!.role == 'Pegawai'
                  ? DashboardPegawai()
                  : user.role == 'Operator'
                      ? DashboardOperator()
                      : DashboardAdmin(),
            );
          });
        } else {
          Info.snackBar('Login Gagal');
        }
      } else {
        Info.snackBar('Request Login Gagal');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }
}
