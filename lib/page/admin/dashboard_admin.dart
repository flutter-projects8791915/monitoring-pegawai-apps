import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:monitoring_pegawai/event/event_pref.dart';

import '../../controller/c_user.dart';
import '../../model/user.dart';

class DashboardAdmin extends StatefulWidget {
  const DashboardAdmin({Key? key}) : super(key: key);

  @override
  State<DashboardAdmin> createState() => _DashboardAdminState();
}

class _DashboardAdminState extends State<DashboardAdmin> {
  CUser _cUser = Get.put(CUser());

  void getUser() async {
    User? user = await EventPref.getUser();
    if (user != null) {
      _cUser.setUser(user);
    }
  }

  @override
  void initState() {
    getUser();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: Center(
        child: Text('Admin'),
      ),
    );
  }
}
