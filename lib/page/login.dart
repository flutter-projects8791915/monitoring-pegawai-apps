import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:monitoring_pegawai/config/asset.dart';

import '../event/event_db.dart';

class Login extends StatelessWidget {
  var _controllerNohp = TextEditingController();
  var _controllerPass = TextEditingController();
  var _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 250,
              width: MediaQuery.of(context).size.width,
              color: Asset.colorPrimary,
              alignment: Alignment.bottomLeft,
              padding: const EdgeInsets.only(
                left: 20,
                bottom: 20,
              ),
              child: Text(
                "Hai\nLogin Dulu",
                style: TextStyle(
                  fontSize: 35,
                  fontWeight: FontWeight.bold,
                  color: Asset.colorPrimaryDark,
                ),
              ),
            ),
            Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    TextFormField(
                      controller: _controllerNohp,
                      validator: (value) =>
                          value == '' ? 'Jangan Kosong' : null,
                      style: TextStyle(
                        color: Asset.colorPrimaryDark,
                      ),
                      decoration: InputDecoration(
                        hintText: '085xxxxxxxxx',
                        hintStyle: TextStyle(
                          color: Asset.colorPrimaryDark,
                        ),
                        fillColor: Asset.colorSecondary,
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: Asset.colorSecondary,
                            width: 1,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: Asset.colorSecondary,
                            width: 2,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: Asset.colorSecondary,
                            width: 1,
                          ),
                        ),
                        prefixIcon: Icon(
                          Icons.phone,
                          color: Asset.colorPrimaryDark,
                        ),
                      ),
                    ),
                    const SizedBox(height: 16),
                    TextFormField(
                      controller: _controllerPass,
                      validator: (value) =>
                          value == '' ? 'Jangan Kosong' : null,
                      style: TextStyle(
                        color: Asset.colorPrimaryDark,
                      ),
                      obscureText: true,
                      decoration: InputDecoration(
                        hintText: '*************',
                        hintStyle: TextStyle(
                          color: Asset.colorPrimaryDark,
                        ),
                        fillColor: Asset.colorSecondary,
                        filled: true,
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: Asset.colorSecondary,
                            width: 1,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: Asset.colorSecondary,
                            width: 2,
                          ),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                          borderSide: BorderSide(
                            color: Asset.colorSecondary,
                            width: 1,
                          ),
                        ),
                        prefixIcon: Icon(
                          Icons.vpn_key,
                          color: Asset.colorPrimaryDark,
                        ),
                      ),
                    ),
                    const SizedBox(height: 30),
                    Material(
                      color: Asset.colorAccent,
                      borderRadius: BorderRadius.circular(30),
                      child: InkWell(
                        onTap: () {
                          if (_formKey.currentState!.validate()) {
                            EventDB.login(
                                _controllerNohp.text, _controllerPass.text);
                            _controllerNohp.clear();
                            _controllerPass.clear();
                          }
                        },
                        borderRadius: BorderRadius.circular(30),
                        child: const Padding(
                          padding: EdgeInsets.symmetric(
                            horizontal: 50,
                            vertical: 12,
                          ),
                          child: Text(
                            'LOGIN',
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
